1. For 32bit executable pass -m32 to CMAKE_C_FLAGS

2. If 32bit code is used then we shoud use int 0x80 as system calls instead of syscall
    Please note int 0x80 and syscall use different registers for parameters.
    32bit print to stdout:
    movl $4, %eax
    movl $1, %ebx
    movl $s, %ecx
    movl $len, %edx
    int $0x80

    64bit print to stdout using syscall:
    mov $1, %ax
    movl $1, %edi
    movl $s, %esi
    movl $len, %edx
    syscall

https://en.wikibooks.org/wiki/X86_Assembly/Interfacing_with_Linux
http://cs.lmu.edu/~ray/notes/linuxsyscalls/
