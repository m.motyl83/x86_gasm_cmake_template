SET(dirlist "")

MACRO(SUBDIRLIST result curdir)
  FILE(GLOB children RELATIVE ${curdir} ${curdir}/*)   
  
  if(EXISTS "${curdir}/${child}/CMakeLists.txt")
  LIST(APPEND dirlist ${curdir})
  endif()
  
  FOREACH(child ${children})
    
    IF(IS_DIRECTORY ${curdir}/${child})

    #MESSAGE( STATUS "DIR:         " ${curdir}/${child} )

    if(EXISTS "${curdir}/${child}/CMakeLists.txt")

    LIST(APPEND dirlist ${curdir}/${child})

    SUBDIRLIST(${result} ${curdir}/${child})
    else()
    SUBDIRLIST(${result} ${curdir}/${child})
    endif()
        
    ELSE()
    
    ENDIF()
  ENDFOREACH()
  SET(${result} ${dirlist})
ENDMACRO()
